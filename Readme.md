# How to build locally

```sh
spectool -g smokeping-prober.spec
rpkg srpm --outdir .
mock -r centos-stream-9-x86_64 --init
mock --enable-network -r centos-stream-9-x86_64 *.src.rpm
```

# How to test

```sh
cp /var/lib/mock/centos-stream-9-x86_64/result/smokeping-prober-0.8.1-3.el9.x86_64.rpm ./tests/data/smokeping-prober.rpm
tmt -vvv run
```