# https://github.com/SuperQ/smokeping_prober
%global goipath         github.com/superq/smokeping_prober
Version:                0.8.1
%global tag             v0.8.1

%gometa -f

%global goname smokeping-prober

%global common_description %{expand:
Prometheus style smokeping prober.}

%global golicenses      LICENSE
%global godocs          CHANGELOG.md README.md

Name:           %{goname}
Release:        1%{?dist}
Summary:        Prometheus style smokeping prober.

License:        Apache-2.0
URL:            %{gourl}
Source0:        %{gosource}
Source1:        %{goname}.sysusers
Source2:        %{goname}.service
Source3:        %{goname}.conf

BuildRequires:  systemd-rpm-macros
Requires(pre): shadow-utils

%description %{common_description}

%gopkg

%prep
%goprep -k
%autopatch -p1

%build
export GO111MODULE=on
%gobuild -o %{gobuilddir}/bin/%{goname} %{goipath}

%install
install -m 0755 -vd                     %{buildroot}%{_bindir}
install -m 0755 -vp %{gobuilddir}/bin/* %{buildroot}%{_bindir}/
install -m 0644 -vpD %{S:1} %{buildroot}%{_sysusersdir}/%{goname}.conf
install -m 0644 -vpD %{S:2} %{buildroot}%{_unitdir}/%{goname}.service
install -m 0644 -vpD %{S:3} %{buildroot}%{_sysconfdir}/default/%{goname}
install -m 0644 -vpD smokeping_prober.yml %{buildroot}%{_sysconfdir}/%{goname}/smokeping_prober.yml

mkdir -vp %{buildroot}/%{_mandir}/man1/
%{buildroot}%{_bindir}/%{goname} --help-man > %{buildroot}/%{_mandir}/man1/%{goname}.1
sed -i '/^  /d; /^.SH "NAME"/,+1c.SH "NAME"\nsmokeping-prober \\- The Prometheus Style Smokeping Prober' \
    %{buildroot}/%{_mandir}/man1/%{goname}.1

%pre
%sysusers_create_compat %{SOURCE1}

%post
%systemd_post %{goname}.service

%preun
%systemd_preun %{goname}.service

%postun
%systemd_postun_with_restart %{goname}.service

%files
%license LICENSE
%doc CHANGELOG.md README.md
%{_bindir}/*
%config(noreplace) %{_sysconfdir}/default/%{goname}
%dir %{_sysconfdir}/%{goname}/
%config(noreplace) %{_sysconfdir}/%{goname}/smokeping_prober.yml
%{_unitdir}/%{goname}.service
%{_sysusersdir}/%{goname}.conf
%{_mandir}/man1/%{goname}.1*

%changelog
%autochangelog
