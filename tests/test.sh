#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
. /usr/share/beakerlib/beakerlib.sh || exit 1

rlJournalStart
    rlPhaseStartSetup
        rlRun "dnf install -y ./data/smokeping-prober.rpm"
        rlRun "cp -f ./data/smokeping_prober.yml /etc/smokeping-prober/smokeping_prober.yml"
        rlServiceStart "smokeping-prober.service"
    rlPhaseEnd

    rlPhaseStartTest
        rlWaitForCmd "curl -s http://localhost:9374/-/healthy" -t 60
        rlRun -s "curl -s -w '%{http_code}' http://localhost:9374/-/healthy"
        rlAssertGrep "200" $rlRun_LOG
    rlPhaseEnd

    rlPhaseStartCleanup
        rlServiceStop "smokeping-prober.service"
        rlRun "dnf remove -y smokeping-prober"
    rlPhaseEnd
rlJournalEnd
